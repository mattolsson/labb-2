//
//  StoryGenerator.h
//  Labb2
//
//  Created by MattiasO on 2015-01-28.
//  Copyright (c) 2015 Mattias Olsson. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface StoryGenerator : NSObject

@property (nonatomic) NSString *fullSaga;
@property (nonatomic) StoryGenerator *story;
@property (nonatomic) NSString *createNewSaga;
- (id)init;
@property (nonatomic) NSArray *animal1;
@property (nonatomic) NSArray *animal2;
@property (nonatomic) NSArray *hobby;
@property (nonatomic) NSArray *adjective2;
@property (nonatomic) NSArray *adjective1;
@property (nonatomic) NSArray *day;
@property (nonatomic) NSArray *place;
@property (nonatomic) NSArray *food;
@property (nonatomic) NSArray *sibling;
@property (nonatomic) NSArray *music;


@end
