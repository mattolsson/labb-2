//
//  ViewController.m
//  Labb2
//
//  Created by MattiasO on 2015-01-26.
//  Copyright (c) 2015 Mattias Olsson. All rights reserved.
//

#import "ViewController.h"
#import "StoryGenerator.h"

@interface ViewController ()
@end

@implementation ViewController



- (IBAction)newSaga:(id)sender {
    StoryGenerator *newSaga = [[StoryGenerator alloc] init];
    self.textField.text = newSaga.createNewSaga;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
