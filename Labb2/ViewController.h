//
//  ViewController.h
//  Labb2
//
//  Created by MattiasO on 2015-01-26.
//  Copyright (c) 2015 Mattias Olsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StoryGenerator.h"

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextView *textField;
@property (weak, nonatomic) IBOutlet UIButton *sagaButton;
@property (nonatomic) NSArray *animal;
@property (nonatomic) NSString *saga;
@end

