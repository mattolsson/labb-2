//
//  StoryGenerator.m
//  Labb2
//
//  Created by MattiasO on 2015-01-28.
//  Copyright (c) 2015 Mattias Olsson. All rights reserved.
//

#import "StoryGenerator.h"

@implementation StoryGenerator

- (id)init {
    return self;
}


- (NSString*) createNewSaga {
    self.animal1 = @[@"hund", @"katt", @"häst", @"gris", @"åsna", @"bäver", @"utter", @"björn"];
    self.animal2 = @[@"delfin", @"valross", @"buffel", @"mask", @"sköldpadda", @"kanin", @"flodhäst", @"tiger"];
    self.hobby = @[@"squash", @"tennis", @"shuffelboard", @"fäktning", @"taekwondo", @"pingis"];
    self.adjective1 = @[@"opassande", @"våldsam", @"töntig", @"fjantig", @"farlig"];
    self.adjective2 = @[@"avundsjuka", @"arga", @"trötta", @"irriterade", @"förbannade", @"glada", @"galna"];
    self.day = @[@"måndag", @"tisdag", @"onsdag", @"torsdag", @"fredag", @"lördag", @"söndag"];
    self.place = @[@"första", @"andra", @"tredje", @"fjärde", @"femte", @"sjätte", @"sjunde"];
    self.food = @[@"köttbullar", @"kebab", @"pizza", @"hamburgare", @"pannkakor", @"semlor", @"godis", @"chips"];
    self.sibling = @[@"bröder", @"systrar", @"syskon"];
    self.music = @[@"Sven Ingvars", @"Spice Girls", @"Mora Träsk", @"Arvingarna", @"One Direction", @"GES", @"Slipnot", @"Beatles", @"Thorleifs", @"Kiss", @"Ghost", @"Iron Maiden", @"Rolands", @"Hoffmaestro"];
    
    NSString* saga = [self fullSaga];
    return saga;
}

- (NSString*)randomElement:(NSArray*)array {
    return array[arc4random() % array.count];
}

- (NSString*) fullSaga {
    NSString* randomAnimal1 = [self randomElement:self.animal1];
    NSString* randomAnimal2 = [self randomElement:self.animal2];
    NSString* randomHobby = [self randomElement:self.hobby];
    NSString* randomAdjective1 = [self randomElement:self.adjective1];
    NSString* randomAdjective2 = [self randomElement:self.adjective2];
    NSString* randomDay = [self randomElement:self.day];
    NSString* randomPlace = [self randomElement:self.place];
    NSString* randomFood = [self randomElement:self.food];
    NSString* randomSibling = [self randomElement:self.sibling];
    NSString* randomMusic = [self randomElement:self.music];
    
    NSString* finalSaga = [NSString stringWithFormat:@"Det var en gång en %@ och en %@. De hade en gemensam hobby som var %@. Ingen av deras mammor var särskilt nöjda med deras val av hobby, eftersom det ansågs %@. Varje dag efter skolan gick de två vännerna hem till %@ för att utöva sin udda hobby. Deras klasskompisar var %@ på de två vännerna, som var sällsynt fanatiska utövare av denna aktivitet. På %@ var det alltid dags för tävling. De två vännerna var taggade till tusen. Just denna dag kom de på %@ plats, och var mycket nöjda med sin insats. Efter tävlingen gick de alltid hem till %@ och åt %@ tillsammans med sina %@ samtidigt som de lyssnade på sitt favoritband %@. ", randomAnimal1, randomAnimal2, randomHobby, randomAdjective1, randomAnimal1, randomAdjective2, randomDay, randomPlace, randomAnimal2, randomFood, randomSibling, randomMusic];
    return finalSaga;
}



@end
